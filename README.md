# TU Bergakademie Freiberg
## Non-linear finite element methods
### Assignment | Summer term 2020

This is a repository of the files belonging to the assignment as a part of the module "Nonlinear Finite Element Methods" in the curriculum of Computational Materials Science. 

For more information, refer [wiki](https://gitlab.com/MukundYedunuthala/nfem-st2020-submission/-/wikis/home).