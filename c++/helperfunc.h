#ifndef FUNC_H
#define FUNC_H


#include<iostream>
#include<vector>
#include<math.h>
using namespace std;

void vec_init( int size, vector<double> &var )
{
    for( int i=0; i<size+1; i++ )
    {
        var.push_back(0);
    }
}

void matinit(std::vector< std::vector<double> > &A, int nofcols)
{
    unsigned int nofrows = A.size();
    for(unsigned int mr = 0; mr < nofrows; mr++){
        // unsigned int nofcols = A[mr].size();
        A[mr].resize(nofcols);
        for(unsigned int mc = 0; mc < nofcols; mc++){
            A[mr][mc] = 0.0;
        }
    }
}
void print_vec( vector<double>::iterator begin, vector<double>::iterator end )
{
    vector<double>::iterator it = begin;
    for ( it; it!=end-1; it=next(it) )
    {
        cout<<*it<<" ";
    }
    cout<<endl;
}
void generate_TSpan(vector<double>::iterator begin, vector<double>::iterator end, double T_start, double T_end, double T_step)
{
    vector<double>::iterator it = next(begin);
    *begin = T_start;
    double t = T_start;
    for ( it; it!=end; it=next(it) )
    {
        t = t+T_step;
        *it = t;
    }
}

#endif