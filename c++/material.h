#ifndef MATERIAL_H
#define MATERIAL_H
#include "helperfunc.h"

void meshGenerator( vector<double>::iterator begin,
                    vector<double>::iterator end,
                    int MESH_REFINEMENT_FACTOR,
                    double INNER_RADIUS,
                    double OUTER_RADIUS,
                    int NUMBER_OF_ELEMENTS )
{
    double Q = pow(MESH_REFINEMENT_FACTOR,(1.0/(NUMBER_OF_ELEMENTS-1)));
    double dR = ( (OUTER_RADIUS-INNER_RADIUS) * (1-Q) )/( 1-(MESH_REFINEMENT_FACTOR*Q) );
    vector<double>::iterator it = begin;
    double rnode = INNER_RADIUS;
    *begin = INNER_RADIUS;
    for ( it = next(begin); it!=end; it=next(it) )
    {
        rnode = rnode + dR;
        *it = rnode;
        dR = dR * Q;
    }
}

void gen_mat_matrix( vector<vector<double>> &C, double UPSILON, double E )
{
    double param = E / ( (1+UPSILON)*(1-2*UPSILON) );
    C[0][0] = ( param * (1-UPSILON) );
    C[0][1] = ( param * UPSILON );
    C[1][0] = ( param * UPSILON );
    C[1][1] = ( param * (1-UPSILON) );
}

#endif