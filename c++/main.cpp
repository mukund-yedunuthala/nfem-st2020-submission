#include "helperfunc.h"
#include "material.h"
int main()
{
    /* INPUT PARAMETERS */
    int NUMBER_OF_ELEMENTS = 10;
    int MESH_REFINEMENT_FACTOR = 2;
    double OUTER_RADIUS = 120;
    double INNER_RADIUS = 60;
    double E = 7e4;
    double UPSILON = 0.30;
    double Q = 35000;
    int TIME_SCALE = 3;
    double P_MAX = 50;
    double START_TIME = 0;
    double LOADING_TIME = 6;
    double END_TIME = 30;
    double TIME_STEP = 0.01;
    int MAX_ITERATIONS = 20;
    double GAUSS_POINT = 0;
    /* MATERIAL MATRIX C */
    vector<vector<double>> C(3);
    matinit(C,3);
    gen_mat_matrix( C, UPSILON, E);
    /* TIME SPAN */
    int TIME_STEPS = (END_TIME - START_TIME)/TIME_STEP;
    vector<double> TIME_SPAN;
    vec_init(TIME_STEPS+1,TIME_SPAN);
    generate_TSpan(TIME_SPAN.begin(), TIME_SPAN.end(), START_TIME, END_TIME, TIME_STEP);
    /* MESHING */
    vector<double> rnodes;
    vec_init( NUMBER_OF_ELEMENTS+1, rnodes );
    meshGenerator( rnodes.begin(), rnodes.end(), MESH_REFINEMENT_FACTOR, INNER_RADIUS, OUTER_RADIUS, NUMBER_OF_ELEMENTS);
    /* NECESSARY FOR SOLVER */
    vector<double> sigma_ov, sigma, global_u, prev_del_u, prev_sigma_ov;
    vec_init( 2, sigma_ov);
    vec_init( 2, sigma );
    vec_init( NUMBER_OF_ELEMENTS+1, global_u );
    vec_init( NUMBER_OF_ELEMENTS+1, prev_del_u);
    vec_init( 2, prev_sigma_ov);

    return 0;
}